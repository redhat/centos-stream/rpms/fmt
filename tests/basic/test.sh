#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
        rlRun "pushd $tmp"
        rlRun "set -o pipefail"
    rlPhaseEnd

    rlPhaseStartTest
        BUILD_PATH=$(rpm -q --qf '%{NAME}-%{VERSION}' fmt)
	echo $BUILD_PATH > output
	rlAssertNotGrep "installed" output
        rlRun "rpmbuild -bc --define \"_sourcedir $TMT_SOURCE_DIR\" --define \"_builddir $TMT_SOURCE_DIR/BUILD\" $TMT_SOURCE_DIR/fmt.spec" 0 "Build"
        rlRun "ctest --test-dir $TMT_SOURCE_DIR/BUILD/$BUILD_PATH/redhat-linux-build --output-on-failure --force-new-ctest-process" 0 "Test"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
    rlPhaseEnd
rlJournalEnd
